package ch.ost.rj.mge.firstapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button buttonActivityTwo;
    private Button buttonStartBrowser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonActivityTwo = findViewById(R.id.main_button_startactivity);
        buttonStartBrowser = findViewById(R.id.button2);

        buttonActivityTwo.setOnClickListener(v -> {
           Intent intent = new Intent(this, SecondActivity.class);
           intent.putExtra(SecondActivity.TEXT_PARAMETER, "Hello from MainActivity");
           startActivity(intent);
        });

        buttonStartBrowser.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ost.ch"));
            startActivity(intent);
        });
    }
}