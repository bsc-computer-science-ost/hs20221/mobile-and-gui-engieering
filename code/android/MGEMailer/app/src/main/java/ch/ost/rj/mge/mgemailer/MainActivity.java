package ch.ost.rj.mge.mgemailer;

import androidx.annotation.RequiresPermission;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final int MIN_PASSWORD_LENGTH = 8;

    private Button loginButton;
    private TextView emailTextView;
    private TextView passwordTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginButton = findViewById(R.id.main_button_login);
        loginButton.setEnabled(false);

        loginButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, WriteEmail.class);
            startActivity(intent);
        });

        emailTextView = findViewById(R.id.main_editText_emailaddress);
        passwordTextView = findViewById(R.id.main_editText_password);


        passwordTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > MIN_PASSWORD_LENGTH) {
                    loginButton.setEnabled(true);
                } else {
                    loginButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
}