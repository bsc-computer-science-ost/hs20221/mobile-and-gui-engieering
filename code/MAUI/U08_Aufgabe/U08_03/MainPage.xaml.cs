﻿namespace U08_03;

public partial class MainPage : ContentPage
{
    public MainPage()
    {
        InitializeComponent();
    }
    
    private void OnHelloButtonClicked(Object sender, EventArgs eventArgs)
    {
        OutputLabelName.Text = "Hello, " + NameEntry.Text;
    }
}

