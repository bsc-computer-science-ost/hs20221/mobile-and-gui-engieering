﻿namespace U09._02;

public partial class MainPage : ContentPage
{
	private int r = 0;
	private int g = 0;
	private int b = 0;
	
	public MainPage()
	{
        InitializeComponent();
	}
	
	private void RSlider_ValueChanged(object sender, ValueChangedEventArgs e)
	{
		r = (int) e.NewValue;
		R.Text = r.ToString();
		UpdateColorBox();
	}
	
	private void GSlider_ValueChanged(object sender, ValueChangedEventArgs e)
	{
		g = (int) e.NewValue;
		G.Text = g.ToString();
		UpdateColorBox();
	}
	
	private void BSlider_ValueChanged(object sender, ValueChangedEventArgs e)
	{
		b = (int) e.NewValue;
		B.Text = b.ToString();
		UpdateColorBox();
	}
	
	private void UpdateColorBox()
	{
		var color = Color.FromRgb(r, g, b);
		ColorArea.Background = new SolidColorBrush(color);
	}
}


