\section{Berechtigungen}
Apps dürfen keine Aktionen ausführen, die andere Dienste negativ beeinflussen können. Die Berechtigungen können von Benutzer zurückgezogen werden. \textit{Wichtig:} vor jedem Zugriff auf geschützte APIs den aktuellen Status prüfen, andernfalls droht eine \textit{SecurityExeption}. Seit API 30 können Berechtigungen vom System automatisch zurückgezogen werden. Beim Anpassen von Berechtigungen kann die App vom System beendet werden.

\textbf{Riskante Operationen}
\begin{itemize}
	\item Zugriff auf System APIs
	\item Zugriff auf sensitive Daten
	\item Zugriff auf bestimmte Hardware
\end{itemize}

Es gibt zwei Arten von Berechtigungen:
\begin{description}
	\item [Normal / Install-time] werden während der Installation beim System angefragt
	\item [Gefährlich / Runtime] werden während der Ausführung beim User angefragt
\end{description}

\subsection{Berechtigungen erteilen} 
\begin{description}
	\item [Bis API 22] Während der Installation der App, kein selektive Ablehnen möglich
	\item [Ab API 23] Während der Nutzung der App, selektives Ablehnen möglich
	\item [Ab API 30] Weiterhin während der Nutzung, Dialog ergänzt um \glqq Einmalig\grqq{} Nutzung
\end{description}

\paragraph{Best Practices} 
\begin{itemize}
	\item Nur Berechtigungen anfordern, die auch wirklich benötigt werden
	\item Im Kontext der Verwendung anfordern, also genau dann, wenn die Berechtigungen benötigt werden.
	\item Transparente Erklärungen
	\item Abbruch ermöglichen
	\item Verweigerung berücksichtigen
\end{itemize}

\subsection{Berechtigungen Anfordern}
Alle benötigten Berechtigungen müssen im Manifest deklariert und später im Code beantragt werden. Dies wird mit dem Knoten \textit{<users-permission>} implementiert.  Zusätzlich ist eine Obergrenze für das API-Level definierbar. 

Zusätzlich sollte für benötigte Features/Hardware deklarierte werden, welche Features die App verwendet. Dies wird im Anschluss dann vom Google Play Store ausgewertet. Dies ist mit dem optionalen Knoten \textit{<uses-feature>} implementiert.

\begin{lstlisting}[caption=Android Berechtigungen]
<uses-permissions
	android:name="android.permissions.CALL_PHONE"/>
	
<uses-feature
	android:name="android.hardware.camera"
	android:required="true" />
\end{lstlisting}

\pagebreak

\paragraph{Anforderung im Code} \hfill \\
Verwendet einen CallBack im Anschluss an die Anforderung an den Benutzer.

\begin{figure}[h!]
  \center
  \includegraphics[width=\textwidth]{androidrequestpermission}
  \caption{Android Berechtigungsanforderungen}
\end{figure}

\section{Persistenz}
Android bietet verschiedene Möglichkeiten Daten persistent auf dem Smartphone zu speichern.

\begin{description}
	\item [App-spezifische Dateien] beliebige Datenformate, für App-internen Gebrauch, Beispiel: Domänenobjekte als JSON 
	\item [Preferences] Key-Value-Paare, für den App-internen gebrauch, Beispiel: Einstellungen des Benutzers
	\item [Datenbanken] strukturierte Daten, für App-internen Gebrauch, Beispiel: Umfangreiche Domänenobjekte
	\item [Medien] Bilder, Dokumente, Musik und Videos, Austausch mit anderen Apps ohne UI-Dialoge, Beispiel: von der App aufgenommene Fotos
	\item [Dokumente] Beliebige Dateiformate, Austausch mit anderen Apps mit UI-Dialogen, Beispiel: Von der App erzeugte PDFs
\end{description}

\paragraph{Speicherarten} \hfill \\
Unter Android wird zwischen zwei verschiedenen Speicherarten unterschieden.

\begin{minipage}{0.5\textwidth}
	\paragraph{Interner Speicher} 
	\begin{itemize}
		\item Stets verfügbar
		\item Geschützter Speicherbereich pro App
		\item Speicherplatz ist begrenzt
		\item Für App-interne Daten
	\end{itemize}
\end{minipage}
\begin{minipage}{0.5\textwidth}
	\paragraph{Externer Speicher} 
	\begin{itemize}
		\item Nicht immer verfügbar
		\item Oft ein Wechseldatenträger erforderlich
		\item Emulation durch Android möglich
		\item Speicherplatz begrenzt
		\item Primär für mit anderen Apps geteilte Daten
	\end{itemize}
\end{minipage}

\begin{figure}[H]
  \center
  \includegraphics[width=0.7\textwidth]{androidspeicher}
  \caption{Mapping von Mechanismen zu Speicherarten}
\end{figure}

\subsection{App-spezifische Dateien}
Habe ihre eigene, proprietäre Dateiformate und können im internen oder externen Speicher abgelegt werden. Die Daten werden bei der Deinstallation der App gelöscht und sind geschützt vor fremden Zugriff. 

\begin{figure}[H]
  \center
  \includegraphics[width=0.5\textwidth]{androidappdateien.png}
  \caption{Android App Dateien}
\end{figure}

\subsection{Preferences}
Preferences sind Key-Value-Paare im internen Speicher. Diese Daten werden bei der Deinstallation der App gelöscht. Es sind keine Berechtigungen nötig.

\begin{figure}[H]
  \center
  \includegraphics[width=0.5\textwidth]{androidpreferences}
  \caption{Android Preferences}
\end{figure}

\subsection{Datenbanken}
Datenbanken bieten die Möglichkeit strukturiere Daten im internen Speicher abzulegen. Auch diese Daten werden bei der Deinstallation der App gelöscht. Es sind keine Berechtigungen nötig. Der Zugriff kann auch zwei Arten realisiert werden: SQLite-API, Room aus AndroidX (SQLIte Wrapper).

\begin{figure}[H]
  \center
  \includegraphics[width=\textwidth]{androidsqliteapi}
  \caption{Android SQLite API}
\end{figure}

\paragraph{Room} \hfill \\
RoomDB ist ein Bestandteil von Android Jetpack und bietet eine Abstraktionsschicht über die SQLite API. Room stellt einen ORM zur Verfügung und basiert auf Java-Annotations. 

\begin{figure}[H]
  \center
  \includegraphics[width=\textwidth]{androidroom}
  \caption{Android Room}
\end{figure}

\subsection{Medien}
Bieten die Möglichkeit Bilder, Dokumente, Musik und Videos auf dem externen Speicher zu speichern und bleiben bei der Deinstallation der App erhalten. Der Zugriff erfolgt über den MediaStore (Content Provider).

\begin{figure}[H]
  \center
  \includegraphics[width=0.5\textwidth]{androidmedien}
  \caption{Android Medien}
\end{figure}

\subsection{Dokumente}
Erlaubt das Zwischenspeicher von beliebigen Dateiformaten auf dem externen Speicher und bleiben bei der Deinstallation der App erhalten.

\begin{figure}[h!]
  \center
  \includegraphics[width=\textwidth]{androiddocuments}
  \caption{Android Dokumente}
\end{figure}

\pagebreak

\section{Hardwarezugriff}
Die heutigen Smartphones verwenden diverse Aktoren und Sensoren, welche von einer App verwendet werden können. Die Qualität der verbauten Hardware variiert stark abhängig vom Hersteller.

\subsection{Sensor Framework}
Ist ein Framework für verschiedene Sensoren: Bewegung, Umgebung, Lage. Alle Sensoren werden gleich verwendet. 

\begin{itemize}
	\item \lstinline|SensorManager| als Einstiegspunkt
	\item \lstinline|Sensor| Als Representant für realen Sensor
	\item \lstinline|SensorEvent| enthält Werte des Sensors
	\item \lstinline|SensorEventListener| für Callbacks
\end{itemize}

Die Genauigkeit sowie auch die Verzögerung des Sensor kann konfiguriert werden. Die genauen Werte können leider nicht aus der Dokumentation gelesen werden. Hier muss berücksichtigt werden, dass dies den Energieverbrauch beeinflusst.

\begin{figure}[H]
  \center
  \includegraphics[width=\textwidth]{androidsensoren}
  \caption{Android Sensoren}
\end{figure}

\subsection{Vibration}
Die Vibrations-Engine kann für das haptisches Feedback verwendet werden. Diese Engine kann über die Klasse \lstinline|Vibrator| angesteuert werden. Dieser Zugriff benötigt die Berechtigung \textit{Vibrate} im Manifest (Muss nicht vom Benutzer bestätigt werden).

\begin{figure}[H]
  \center
  \includegraphics[width=0.5\textwidth]{androidvibrate}
  \caption{Android Vibration}
\end{figure}

\subsection{REST-Kommunikation}
Für einfache HTTP/REST-Calls existieren viele Varianten, wie diese realisiert werden können.

\begin{description}
	\item [HttpsURLConnection] ist Teil der Android SKD und die Verwendung ist eher kompliziert.
	\item [OkHttp] ist eine oft verwendete, effiziente Alternative und verfügbar ab API 21.
	\item [Retrofit] erlaubt die Definition von Endpunkten und Datenobjekten, verwendet OkHttp zur Kommunikation, verwendet unterschiedliche Konverter für die Serialisierung
\end{description}

\begin{figure}[H]
  \center
  \includegraphics[width=\textwidth]{okhttp}
  \caption{OkHttp und Gson REST-Request}
\end{figure}

\begin{figure}[H]
  \center
  \includegraphics[width=\textwidth]{retrofit}
  \caption{Retrofit REST-Request}
\end{figure}

\paragraph{Status der Internet-Verbindung} \hfill \\
Den Status der Internet-Verbindung kann über die Klasse \lstinline|ConnectivityManager| abgefragt werden. Üblicherweise werden zwei Kanäle verwendet: Mobile, Wifi. Android nutzt automatisch den besten Kanal. Die Statusänderungen werden über einen Broadcast übermittelt.

\subsubsection{Positionsbestimmung}
Es gibt zwei Möglichkeiten die Lokation eines Smartphones zu bestimmen. Beide haben eine ähnliche API und benötigen dieselben Berechtigungen.

\begin{itemize}
	\item Ohne Google Services: \textit{LocationManager}
	\item Mit Google Services: Fused Location Provider
\end{itemize}

\begin{figure}[H]
  \center
  \includegraphics[width=0.5\textwidth]{androidposition}
  \caption{Android Position Bestimmen}
\end{figure}


\subsubsection{Kamera}

Möglichkeiten für Fotos und Videos:
\begin{enumerate}
	\item Kamera-App via Intents starten
	\item Camera-API
	\item Camera2-API
	\item CameraX-API (AndroidX)
\end{enumerate}

Die Option 1 hat den Vorteil, dass keine Berechtigungen nötig sind und weniger Logik verwendet werden muss. Die Vorteile von Optionen 2 bis 4 sind, dass die Kamera-Funktionalität Teil der eigenen App ist, was mehr Möglichkeiten und Kontrolle bietet.
