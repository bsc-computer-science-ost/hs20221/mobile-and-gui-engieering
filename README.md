# Mobile und GUI Engineering

Dieses Repository beinhaltet die erarbeiteten Unterlagen für den Mobile und GUI Engineering Kurs and FH OST im HS2022.

**Inhalt:**

Android

- Activites, Intents, Resources
- Layouts, Widgets, Events
- Fragments, Styling
- Persistenz, Hardwarezugriff
- Obeserver Pattern, Deployment

.NET MAUI

- Layouts und Views
- Ressources, Styling, Theming
- Data Binding
- MVVM
- Solution Design, Threading
- Windows Platform Framework

## Lernziele

- [X] Sie kennen und verstehen die Grundkonzepte einer Android-App.
- [X] Sie können selbständig ein "Hello World"-App erstellen und erweitern.
- [X] Sie wissen, wie Sie Kompabilität mit alten Geräten erreichen können.
- [X] Sie kennen den Unterschied zwischen einem Layout und einem Widget.
- [X] Sie verstehen einfache Layouts und können diese selbständig anwenden.
- [X] Sie verstehen Adapter-basierte Layouts und können diese selbständig anwenden.
- [X] Sie kennen die üblichen Widgets und deren Anwendung.
- [X] Sie können Fragments korrekt einsetzen.
- [X] Sie wissen, was Material Design umfasst.
- [X] Sie sind in der Lage, die Grundprinzipien von Material Design korrekt anzuwenden.
- [X] Sie können mit Themes und Styles das Aussehen von Apps verändern.
- [X] Sie kennen unterschiedliche Arten von Berechtigungen.
- [X] Sie können Berechtigungen anfordern.
- [X] Sie können geeignete Methoden zur Persistierung von Daten anwenden.
- [X] Sie kennen Beispiele für den Zugriff auf die Hardware eines Smartphones.
- [X] Sie verstehen die Schichtenarchitektur.
- [X] Sie können das Observer Pattern anwenden.
- [X] Sie können das MVC-Pattern erklären.
- [X] Sie haben eine grobe Vorstellung davon, was sich hinter MVP und MVVM verbirgt.
- [X] Sie wissen, wozu Sie Broadcasts und Services in Android-Apps einsetzen können.
- [X] Sie können den Build-Prozess von Android und relevante Dateiformate erklären.
- [X] Sie können die Ziele und Inhalte von Android Jetpack erklären.
- [X] Sie kennen den Einsatzzweck verschiedener Architecture Components.
- [X] Sie kennen Gemeinsamkeiten von Android und .NET MAUI.
- [X] Sie kennen und verstehen die wichtigsten Konzepte von Microsoft .NET.
- [X] Sie haben verstanden, wie eine einfache .NET MAUI-Anwendung aufgebaut ist.
- [X] Sie können einfache Programme in der Programmiersprache C# schreiben.
- [X] Sie kennen die wichtigsten Konzepte in XAML und können diese erklären.
- [X] Sie kennen die wichtigsten Klassen aus der .NET MAUI-Control Library.
- [X] Sie können in den Übungen selbständig Pages, Layouts und Views einsetzen.
- [X] Sie kennen einige MAUI-Views und -APIs, die Ihnen bei der visuellen Gestaltung ihres GUI helfen.
- [X] Sie wissen, wie Sie das Aussehen von MAUI-Apps effizient gestalten können.
- [X] Sie kennen die Gemeinsamkeiten von Data Binding in Android und .NET MAUI.
- [X] Sie können mittels Data Binding MAUI-Views mit C#-Klassen verknüpfen.
- [X] Sie kennen die in .NET MAUI verwendeten Varianten des Observer Patterns.
- [X] Sie haben die Bestandteile von MVVM verstanden und können diese erklären.
- [X] Sie können MVVM in .NET MAUI umsetzen.
- [X] Sie kennen Hilfsmittel für die Umsetzung von MVVM in .NET MAUI.
- [X] Sie können in .NET MAUI-Solutions mit Projekten sinnvoll strukturieren.
- [X] Sie können technische Details mit geeigneten Mitteln isolieren.
- [X] Sie wissen, wie Sie die Blockierung der grafischen Oberfläche vermeiden können.
- [X] Sie wissen, wie Sie Mehrsprachigkeit umsetzen können.
- [X] Sie wissen, wie Sie eigene Controls in .NET  MAUI erstellen und verwenden können.
- [X] Sie können die Shell in einfachen MAUI-Applikationen einsetzen.
- [X] Sie kennen Gemeinsamkeiten und Unterschiede von MAUI und WPF.
- [X] Sie kennen spannende Trends und Technologien für die nahe Zukunft.

